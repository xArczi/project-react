import ReactDOM from 'react-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import './App.css';
import '../src/components/Icons'
import Homepage from './pages/Homepage';
import Header from './components/Header';


function App() {
  
  return (
    <div>
      <Homepage />
    </div>
  );
}

export default App;
