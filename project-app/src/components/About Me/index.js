import React from 'react';
import dev_icon from '../../assets/dev_icon.png';
import bit_bucket_icon from '../../assets/bit_bucket_icon.png'
import cv from "../../assets/cv.jpg"
import koła_01 from "../../assets/koła_01.png"

const AboutMe = () =>{
    return(
    <div class="about-me">
        <div class="about-me-txt">
                <h2>//Hi, im artur</h2>
                <h3>Developer front-end</h3>
                
                <span>facfafeacfecawcfaecawcwaefcwcfwc</span>
                
                <span>cewaecgcawgeacgeacgawca</span>
                
                <div class="about-me-works">
                    <span class="works">see my works</span>
                    <img class="dev_icon" src={dev_icon} ></img>
                    <img src={bit_bucket_icon} alt=""/>
                </div>
            
        </div>
        <div class="cv">
            <img src={cv} width="600" height="530" alt="zdjecie cv"/>
            <img class="kolo" width="500" height="530" src={koła_01} alt=""/>
        </div>
        <div class="contact-me">
            <h2>im freelancer</h2>
            <span>contact me if you want to work with me</span>
            
            <div class="buttons">
                <div class="button1">
                    <p class="p-hire-me"><span class="Hire-me">Hire me!</span></p>
                </div>
                <div class="button2">
                    <p class="p-dw-me"><span class="dw-me">Download CV</span></p>
                </div>
            </div>    
        </div>
        <div>
            <img class="kolo1" width="500" height="530" src={koła_01} alt=""/>
        </div>
        
    </div>
    )
}

export default AboutMe