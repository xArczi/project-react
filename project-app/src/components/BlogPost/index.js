import React from 'react';
import portfolio_case_01 from '../../assets/portfolio_case_01.png'


const BlogPost = () => {
    return(
        <div class="blogs-wrapper">
        <h2>Blog posts</h2>
        <h3>Hints and tips</h3>
        <div class="blogs">
            <div>
                <img src={portfolio_case_01} alt=""/>
            </div>
            <div>
                <h2>title 01</h2>
                <h3>secondary title</h3>
                <p><span>he majority have suffered alteration in some form, by injected humour, 
                    or randomised words which don't look even slightly believable. If you are going to use a
                     passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. 
                     All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first 
                     true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, 
                    to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always fr</span></p>
            </div>
        </div>
        <div class="blogs">
            <div>
                <img src={portfolio_case_01} alt=""/>
            </div>
            <div>
                <h2>title 01</h2>
                <h3>secondary title</h3>
                <p><span>he majority have suffered alteration in some form, by injected humour, 
                    or randomised words which don't look even slightly believable. If you are going to use a
                     passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. 
                     All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first 
                     true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, 
                    to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always fr</span></p>
            </div>
        </div>
    </div>
    )
}

export default BlogPost;