import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'

const Header = () => {
    
    return(
        <div class="nav-wrapper">
        <div class="LOGO">
            <p id="ap"><span class="ap1">A.P.</span></p>
    
        </div>
        <div>

            <ul class="main-nav">
                <li>About me</li>
                <li>Skills</li>
                <li>Portfolio</li>
                <li>Blog</li>
                <li>Contact me</li>
                <li>|</li>
                <li><FontAwesomeIcon icon={['fab', 'twitter']} /></li>
                <li><FontAwesomeIcon icon={['fab', 'facebook']} /> </li>
                <li><FontAwesomeIcon icon={['fab', 'linkedin']} /></li>
            </ul>
        </div>

    </div>
    )
}

export default Header