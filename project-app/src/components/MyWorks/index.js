import React from 'react';
import portfolio_case_01 from '../../assets/portfolio_case_01.png'
import portfolio_case_02 from '../../assets/portfolio_case_02.png'
import portfolio_case_03 from '../../assets/portfolio_case_03.png'
import portfolio_case_04 from '../../assets/portfolio_case_04.png'
import portfolio_case_05 from '../../assets/portfolio_case_05.png'
import portfolio_case_06 from '../../assets/portfolio_case_06.png'


const MyWorks = () => {
    return (
        <div class="my-works-main-wrapper">
        <h2>My works</h2>
        <h3>Portfolio</h3>
        <p><span>fafeacfecawcfaecawcwaefcfafeacfecawcfaecawcwaefcfafeacfecawcfaecawcwaefcfafeacfecawcfaecawcwaefcfafeacfecawcfaecawcwaefcfafeacfecawcfaecawcwaefcfafeacfecawcfaecawcwaefc</span></p>
        <div class="portfolio_case">
            <div>
                <img src={portfolio_case_01} alt=""/>
            </div>
            <div>
                <img src={portfolio_case_02} alt=""/>
            </div>
            <div>
                <img src={portfolio_case_03} alt=""/>
            </div>
            <div>
                <img src={portfolio_case_04} alt=""/>
            </div>
            <div>
                <img src={portfolio_case_05} alt=""/>
            </div>
            <div>
                <img src={portfolio_case_06} alt=""/>
            </div>
        </div>

    </div>
    )
}

export default MyWorks