import React from "react";
import easy_code_button from '../../assets/easy_code_button.png'

const Skills = () =>{
    return(
    <div class="main-descr">
        <div class="descr">
            <h2>//About me</h2>
            <h3>All about Techy</h3>
            
            <p><span>Etiam pulvinar fringilla mi, ut rhoncus magna.
                
                <p>Curabitur fermentum nunc lacus, quis commodo erat condimentum in. Praesent ullamcorper placerat purus, sed auctor lacus bibendum sed. Donec bibendum tincidunt eleifend. Nam convallis vulputate nisi, sit amet bibendum leo venenatis et. Nulla facilisi. Quisque a enim arcu. Mauris eget viverra lectus.</p>
                
            Nulla nec est nisl. Proin nec euismod nisl, at maximus sem. Donec sed consequat mauris. Maecenas urna massa, posuere vel dapibus ac, tristique et ex. Phasellus semper est eget odio feugiat ultrices at ac eros. Mauris et felis libero.</span></p>
            <h3>My interests</h3>
            <ul>
                <li>Music</li>
                <li>el1</li>
                <li>el2</li>
            </ul>
            <div class="easybutton">
                <p><span>Ukończyłem kurs Easy Code</span></p>
                <img class="easycodebutton" src={easy_code_button} alt=""/>
            </div>

        </div>
        <div class="skills">
            <h2>//Skills</h2>
            <p><span>rmentum nunc lacus, quis comrmentum nunc lacus, quis commodo erat condimentum in. Praesent ullamcorper placerat purus, sed auctor lacus bibmodo erat condimentum in.rmentum nunc lacus, quis commodo erat condimentum in. Praesent ullamcorper placerat purus, sed auctor lacus bib Praesent ullamcorper placerat purus, sed auctor lacus bib</span></p>
            
            <div class="wykres-back">
                <p id="wykres1">PMP 100%</p>
            </div>
            <div class="wykres-back">
                <p id="wykres2">JS 90%</p>
            </div>
            <div class="wykres-back">
                <p id="wykres3">HTML 90%</p>
            </div>
            <div class="wykres-back">
                <p id="wykres4">NODEJS 60%</p>
            </div>
            <div class="wykres-back">
                <p id="wykres5">CSS 90%</p>
            </div>
            <div class="wykres-back">
                <p id="wykres6">GO 60%</p>
            </div>
            
        </div>
    </div>
    )
}

export default Skills