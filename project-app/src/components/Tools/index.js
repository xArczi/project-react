import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import icon_square_big from '../../assets/icon-square-big.png'
import styled_comps from '../../assets/styled_comps.png'
import redux_icon from '../../assets/redux_icon.png'
import flexbox_icon from '../../assets/flexbox_icon.png'
import tool_program_ikona from '../../assets/tool_program_ikona.png'

const Tools = () =>{
    return(
        <div class="tools-wrapper">
        <h2>//TOOLS</h2>
        <h2>My Essentials</h2>
        <div class="tools">
            <div class="react_icon">
                <FontAwesomeIcon icon={['fab', 'react']} size="5x" color="blue"/>
                <span>React</span>
                <span>16.6.3</span>
            </div>
            <div class="icon-square_icon">
                <img src={icon_square_big} class="icon-square" alt=""/>
                <span>Redux</span>
                <span>4.16.4</span>
            </div>
            <div class="express_icon">
                <FontAwesomeIcon icon={['fab', 'node-js']} size="5x" />
                <span>Express</span>
                <span>4.16.4</span>
            </div>
            <div class="styled_comps_icon">
                <img src={styled_comps} class = "styled_comps" alt=""/>
                <span>Styled Components</span>
                <span>4.16.4</span>
            </div>
            <div class="redux_icon">
                <img src={redux_icon} class="redux" alt=""/>
                <span>Redux</span>
                <span>4.16.4</span>
            </div>
            <div class="flexbox_icon">
                <img src={flexbox_icon} class="flexbox" alt=""/>
                <span>Redux</span>
                <span>4.16.4</span>
            </div>
            <div class="tool_program_icon">
                <img src={tool_program_ikona} class="tool_program" alt=""/>
                <span>Redux</span>
                <span>4.16.4</span>
            </div>
            <div class="tool_program_icon2">
                <img src={tool_program_ikona} class="tool_program2" alt=""/>
                <span>Redux</span>
                <span>4.16.4</span>
            </div>
        </div>


    </div>
    )
}

export default Tools