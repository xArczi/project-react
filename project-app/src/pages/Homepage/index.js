import React from 'react'
import AboutMe from '../../components/About Me'
import BlogPost from '../../components/BlogPost'
import Contact from '../../components/Contact'
import Header from '../../components/Header'
import MyWorks from '../../components/MyWorks'
import Skills from '../../components/Skills'
import Tools from '../../components/Tools'


const Homepage = () => {
    return(
        <div>
            <Header/>
            <AboutMe/>
            <Skills/>
            <Tools/>
            <MyWorks/>
            <BlogPost/>
            <Contact/>
        </div>
    )
}
export default Homepage